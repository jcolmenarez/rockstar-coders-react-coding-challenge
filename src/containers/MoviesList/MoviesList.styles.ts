export function getMoviesListWrapperCustomStyles(): string {
  return `
    & {
      display: grid;
      gap: 2rem 4rem;
      grid: repeat(5, 26rem) / repeat(4, 1fr);

      h1 {
        color: #4d4b56;
        grid-column: 1 / span 4;
        text-align: center;
      }
    }
  `;
}
