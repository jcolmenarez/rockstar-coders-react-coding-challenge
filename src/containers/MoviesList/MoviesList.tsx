import styled from 'styled-components';
import { MovieLink, MovieDataProps } from '../../components';
import { getMoviesListWrapperCustomStyles } from './MoviesList.styles';

const StyledMoviesListWrapper = styled('div')`
  ${getMoviesListWrapperCustomStyles}
`;

export interface MoviesListProps {
  movies: null | MovieDataProps[];
}

export function MoviesList({ movies }: MoviesListProps): JSX.Element {
  return Array.isArray(movies) ? (
    <StyledMoviesListWrapper>{renderMoviesSet(movies)}</StyledMoviesListWrapper>
  ) : (
    <p>Loading...</p>
  );
}

function renderMoviesSet(moviesSet: MovieDataProps[]) {
  return moviesSet.length ? (
    moviesSet.map((movieProps) => {
      return <MovieLink key={`movie-${movieProps.id}`} {...movieProps} />;
    })
  ) : (
    <h1>There are not movies results to show</h1>
  )
}
