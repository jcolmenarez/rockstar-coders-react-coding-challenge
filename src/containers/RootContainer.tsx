import React from 'react';
import styled from 'styled-components';

const StyledRootWrapper = styled('div')`
  & {
    background-color: #f7f7f8;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    min-height: 100vh;
    margin: -8px -8px 3.5rem;
    padding: 0 0 3.5rem;
    position: relative;
    width: 100vw;
  }
  footer {
    background-color: #050409;
    bottom: 0;
    color: #4d4b56;
    height: 3rem;
    margin: 0;
    max-height: 3rem;
    min-height: 3rem;
    padding: 1rem;
    position: fixed;
    text-align: center;
    width: 100%;
  }
`;

interface RootContainerProps {
  children: React.ReactNode;
}

export function RootContainer({ children }: RootContainerProps): JSX.Element {
  return (
    <StyledRootWrapper>
      {children}
      <footer>
        <p>{'RockStar Coders - React Coding Challenge by Javier Colmenárez - 2021'}</p>
      </footer>
    </StyledRootWrapper>
  );
}
