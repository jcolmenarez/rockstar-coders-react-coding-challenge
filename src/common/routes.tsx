import { Route, Switch } from 'react-router-dom';
import { DetailsPage, DiscoveryPage, NotFoundPage } from '../pages';

export function Routes(): JSX.Element {
  return (
    <Switch>
      <Route exact path="/" component={DiscoveryPage} />
      <Route path="/movie/:movieId" component={DetailsPage} />
      <Route path="/*" component={NotFoundPage} />
    </Switch>
  );
}
