export const DISCOVERY_URL = 'https://api.themoviedb.org/3/discover/movie';
export const IMAGES_URL = 'https://image.tmdb.org/t/p/original/';
export const SEARCH_URL = 'https://api.themoviedb.org/3/search/movie';
export const TMDB_API_KEY = 'd34f6af7a394cb747da236bd910aaa08';
