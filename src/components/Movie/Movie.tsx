import { Fragment } from 'react';
import { Link, useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { IMAGES_URL } from '../../common';
import { getMovieLinkCustomStyles, getMovieWrapperCustomStyles } from './Movie.styles';

const StyledMovieLink = styled(Link)`
  ${getMovieLinkCustomStyles}
`;

const StyledMovieWrapper = styled('div')`
  ${getMovieWrapperCustomStyles}
`;

export interface MovieDataProps {
  id: number;
  overview: string;
  popularity: number;
  poster_path: null | string;
  release_date: string;
  title: string;
  vote_average: number;
  vote_count: number;
}
export interface MovieProps extends MovieDataProps {}

export function MovieLink(props: MovieProps): JSX.Element {
  return (
    <StyledMovieLink to={{ pathname: `/movie/${props.id}`, state: { ...props, isSummaryLayout: true } }}>
      <MovieCommonContent {...props} />
    </StyledMovieLink>
  );
}

interface MovieSummaryRouterProps extends MovieDataProps {}
export function MovieSummary(): JSX.Element {
  const location = useLocation<MovieSummaryRouterProps>();
  const { ...props } = location.state;
  return (
    <StyledMovieWrapper>
      <MovieCommonContent {...props} />
      <p>{props.overview}</p>
    </StyledMovieWrapper>
  );
}

function MovieCommonContent({ poster_path, release_date, title, vote_average }: MovieProps): JSX.Element {
  const year = release_date.substr(0, 4);
  return (
    <Fragment>
      {!poster_path ? null : <img alt={`${title}'s poster`} src={`${IMAGES_URL}${poster_path}`} />}
      <div className="Movie-infoWrapper">
        <span className="Movie-title">{`${title} - (${year})`}</span>
        <span className="Movie-rating">
          {[...Array(5)].map((star, i) => {
            return <i className={i * 2 <= vote_average ? 'active' : ''} />;
          })}
        </span>
      </div>
    </Fragment>
  );
}
