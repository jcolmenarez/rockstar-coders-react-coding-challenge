const movieCommonStyles = `
  & {
    align-items: center;
    background-color: #4d4b56;
    border-radius: .5rem;
    color: #4d4b56;
    display: flex;
    flex-direction: column;
    position: relative;
    overflow: hidden;

    .Movie-rating {
      display: flex;

      i + i {
        margin-left: 0.25rem;
      }

      i::before {
        content: "\\2605";
        opacity: .35;
      }
      i.active::before {
        opacity: 1;
      }
    }
  }
`;

export function getMovieLinkCustomStyles(): string {
  return `
    ${movieCommonStyles}
    box-shadow: 0 0 8px 4px rgba(0, 0, 0, .25);
    height: 26rem;
    max-height: 26rem;
    min-height: 26rem;
    text-decoration: none;
    transform: scale(1);
    transition: all 250ms ease-in;
    width: 100%;

    img {
      display: block;
      margin: 0 auto;
      max-width: 100%;
      min-width: 100%;
      position: absolute;
      opacity: .75;
      transition: all 250ms ease-in;
      width: 100%;
      z-index: 1;
    }

    .Movie-infoWrapper {
      align-items: center;
      background-color: #d9502c;
      box-sizing: border-box;
      color: white;
      display: flex;
      flex-direction: column;
      height: 6rem;
      justify-content: center;
      margin-top: auto;
      max-height: 6rem;
      min-height: 6rem;
      opacity: .9;
      padding: 0.75rem 1rem;
      transition: all 250ms ease-in;
      width: 100%;
      z-index: 2;

      span {
        display: block;
        font-size: .85rem;
        text-align: center;

        &.Movie-title {
          font-size: 1.15rem;
          margin-bottom: .5rem;
        }
      }
    }

    &:hover {
      transform: scale(1.05);

      img {
        opacity: 1;
      }

      .Movie-infoWrapper {
        opacity: 1;
      }
    }
  `;
}

export function getMovieWrapperCustomStyles(): string {
  return `
    ${movieCommonStyles}
    background-color: transparent !important;
    border-radius: 0 !important;
    margin: 0;
    padding: 0;

    img {
      height: calc(60vh - 56px);
      width: auto;
    }

    .Movie-infoWrapper {
      margin-top: 2rem;
    }

    .Movie-title {
      font-size: 1.5rem;
      margin-bottom: .5rem;
    }

    .Movie-rating {
      color: #4d4b56;
      margin: .5rem 0;
    }

    p {
      font-size: 1rem;
      line-height: 1.5;
      max-width: 40vw;
      text-align: center;
    }
  `;
}
