export function getRatingFilterCustomStyles(): string {
  return `
    & {
      color: white;
      margin-left: 2rem;

      button {
        border: none;
        margin: 0;

        &:first-of-type {
          margin-left: .5rem;
        }
      }

      i::before {
        color: #4d4b56;
        content: "\\2605";
        transition: all 250ms ease-in;
      }

      .active i::before {
        color: #d9502c;
      }

      button:hover {
        cursor: pointer;
      }
    }
  `;
}
