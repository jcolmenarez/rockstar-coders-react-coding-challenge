import styled from 'styled-components';
import { getRatingFilterCustomStyles } from './RatingFilter.styles';

const StyledRatingFilterWrapper = styled('div')`
  ${getRatingFilterCustomStyles}
`;

export interface RatingFilterProps {
  rating: null | number;
  ratingHandler: (newRate: number) => void;
}

export function RatingFilter({ rating, ratingHandler }: RatingFilterProps): JSX.Element {
  const ratingAvg = rating !== null ? Math.round(rating / 2) : 0;
  return (
    <StyledRatingFilterWrapper>
      <span>{'Filter by Rating:'}</span>
      {[...Array(5)].map((starEl, i) => {
        i += 1;
        return (
          <button key={`rating-star-${i}`} className={rating && i <= ratingAvg ? 'active' : ''} onClick={(evt) => ratingHandler(i*2)} type="button"><i /></button>
        );
      })}
    </StyledRatingFilterWrapper>
  );
}
