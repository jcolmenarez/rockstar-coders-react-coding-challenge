import { useRef, useState } from 'react';
import styled from 'styled-components';
import { getSearchBoxCustomStyles } from './SearchBox.styles'

const StyledSearchBox = styled('div')`
  ${getSearchBoxCustomStyles}
`;

export interface SearchBoxProps {
  onChange: (term: string) => void;
}

export function SearchBox({ onChange }: SearchBoxProps): JSX.Element {
  const [showClear, setShowClear] = useState(false);
  const searchFieldRef = useRef<HTMLInputElement>(null);

  function onChangeHandler() {
    setShowClear(!!searchFieldRef.current?.value);
    onChange(searchFieldRef.current?.value ?? '');
  }

  function onClearSearchBox() {
    if (searchFieldRef.current) {
      searchFieldRef.current.value = '';
      onChangeHandler();
    }
  }

  return (
    <StyledSearchBox>
      <input id="movie-search-box" name="movie-search-box" onChange={onChangeHandler} placeholder="Search for a movie" ref={searchFieldRef} type="text" />
      <button className={showClear ? 'visible' : ''} onClick={onClearSearchBox} type="button">&times;</button>
    </StyledSearchBox>
  );
}

