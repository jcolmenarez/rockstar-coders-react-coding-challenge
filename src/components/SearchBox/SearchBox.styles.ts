export function getSearchBoxCustomStyles(): string {
  return `
    & {
      align-items: center;
      display: flex;
    }

    input {
      appearance: auto;
      box-sizing: border-box;
      font-size: 1rem;
      line-height: 1.25;
      min-height: 1.5rem;
      outline: none;
      padding: .25rem .5rem;
    }

    input + button {
      background-color: transparent;
      border: none;
      color: white;
      font-size: 2.5rem;
      line-height: 1;
      opacity: 0;
      transition: all 250ms ease-in;

      &.visible {
        opacity: 1;

        &:hover {
          cursor: pointer;
        }
      }
    }
  `;
}
