import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { getNotFoundPageWrapperCustomStyles } from './NotFoundPage.styles';

const StyledNotFoundPageWrapper = styled('div')`
  ${getNotFoundPageWrapperCustomStyles}
`;

export function NotFoundPage(): JSX.Element {
  return (
    <StyledNotFoundPageWrapper>
      <Link to="/">{'< Go back to home page'}</Link>
      <h1>Page not found</h1>
    </StyledNotFoundPageWrapper>
  );
}
