import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { MovieSummary } from '../../components';
import { getDetailsPageContainerCustomStyles } from './DetailsPage.styles';

const StyledContainer = styled('div')`
  ${getDetailsPageContainerCustomStyles}
`;

export interface DetailsPageProps {}

export function DetailsPage({}: DetailsPageProps): JSX.Element {
  return (
    <StyledContainer>
      <section className="DetailsPage-header">
        <Link to="/">{'< Go back'}</Link>
      </section>
      <MovieSummary />
    </StyledContainer>
  );
}
