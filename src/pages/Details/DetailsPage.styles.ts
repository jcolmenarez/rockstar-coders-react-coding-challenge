export function getDetailsPageContainerCustomStyles(): string {
  return `
    & {
      align-items: center;
      display: flex;
      flex-direction: column;
      position: relative;
      width: 100%;
    }

    .DetailsPage-header {
      align-items: center;
      background-color: #050409;
      display: flex;
      justify-content: center;
      max-width: 100%;
      min-width: 100%;
      padding: 1rem;
      width: 100%;

      a {
        color: white;
        font-size: 1.25rem;
        margin: 0 auto 0 12.5rem;
        text-decoration: none;
      }
    }
  `;
}
