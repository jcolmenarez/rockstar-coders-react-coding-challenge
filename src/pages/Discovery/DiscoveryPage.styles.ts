export function getDetailsPageWrapperCustomStyles(): string {
  return `
    & {
      align-items: center;
      display: flex;
      flex-direction: column;
      position: relative;
      width: 100%;

      .DiscoveryPage-header {
        align-items: center;
        background-color: #050409;
        display: flex;
        justify-content: center;
        max-width: 100%;
        min-width: 100%;
        padding: 1rem;
        width: 100%;
      }

      .DiscoveryPage-content {
        margin: 0 auto;
        max-width: 75vw;
        min-width: 75vw;
        padding-top: 2rem;
        width: 75vw;
      }
    }
  `;
}
