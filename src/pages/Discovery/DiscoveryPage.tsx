import { useEffect, useState } from 'react';
import styled from 'styled-components';
import { DISCOVERY_URL, SEARCH_URL, TMDB_API_KEY } from '../../common';
import { MovieDataProps, RatingFilter, SearchBox } from '../../components';
import { MoviesList } from '../../containers';
import { getDetailsPageWrapperCustomStyles } from './DiscoveryPage.styles';

const StyledDiscoveryPageWrapper = styled('div')`
  ${getDetailsPageWrapperCustomStyles}
`;

interface DiscoveryPage {}

export function DiscoveryPage({}: DiscoveryPage): JSX.Element {
  const [movies, setMovies] = useState<null | MovieDataProps[]>(null);
  const [originalMoviesSet, setOriginalMoviesSet] = useState<MovieDataProps[]>([]);
  const [rating, setRating] = useState<null | number>(null);
  const [searchTerm, setSearchTerm] = useState<null | string>(null);

  useEffect(() => {
    fetchData();

    async function fetchData() {
      const response = await fetch(`${DISCOVERY_URL}?api_key=${TMDB_API_KEY}`);
      const { results }: { results: MovieDataProps[] } = await response.json();
      setOriginalMoviesSet(results);
      updateMoviesSet(results);
    }
  }, []);

  useEffect(() => {
    if (!searchTerm) {
      setRating(null);
      updateMoviesSet(originalMoviesSet);
    } else {
      queryMovie(searchTerm);
    }

    async function queryMovie(term: string) {
      const response = await fetch(`${SEARCH_URL}?api_key=${TMDB_API_KEY}&query=${term}`);
      const { results }: { results: MovieDataProps[] } = await response.json();
      updateMoviesSet(results);
    }
  }, [searchTerm]);

  function searchHandler(term: string) {
    if (!term) {
      setRating(null);
      setSearchTerm(null);
      updateMoviesSet(originalMoviesSet);
    } else {
      setSearchTerm(term);
    }
  }

  function rateMovie(newRating: number): void {
    if (rating === newRating) {
      setRating(null);
      updateMoviesSet(originalMoviesSet);
    } else {
      const newMoviesSet = originalMoviesSet.filter(({ vote_average }) => (vote_average >= newRating - 2 && vote_average <= newRating));
      setRating(newRating);
      updateMoviesSet(newMoviesSet);
    }
  }

  function updateMoviesSet(moviesSet: MovieDataProps[]): void {
    moviesSet.sort((a, b) => b.popularity - a.popularity);
    setMovies(moviesSet);
  }

  return (
    <StyledDiscoveryPageWrapper>
      <section className="DiscoveryPage-header">
        <SearchBox onChange={searchHandler} />
        <RatingFilter rating={rating} ratingHandler={rateMovie} />
      </section>
      <section className="DiscoveryPage-content">
        <MoviesList movies={movies} />
      </section>
    </StyledDiscoveryPageWrapper>
  );
}
