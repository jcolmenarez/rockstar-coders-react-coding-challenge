import { BrowserRouter as Router } from 'react-router-dom';
import { Routes } from './common';
import { RootContainer } from './containers';

function App() {
  return (
    <RootContainer>
      <Router>
          <Routes />
      </Router>
    </RootContainer>
  );
}

export default App;
